extends KinematicBody2D
#extends Node

export(float) var SPEED = 200.0

enum STATES { IDLE, FOLLOW }
var _state = null

var path = []
var target_point_world = Vector2()
var target_position = Vector2()

var velocity = Vector2()

var bHover = false
var bSelect = false

var dungeonmanagement

var DisplayClass = preload("res://prefabs/DisplayNameUI.tscn")
var displayObj

func _ready():
	_change_state(IDLE)
	dungeonmanagement = get_parent().get_node('/root/Game')
	
	#displayObj = DisplayClass.instance()
	#get_node('/root/Game/Scene/Characters').add_child(displayObj)
	


func _change_state(new_state):
	if new_state == FOLLOW:
		path = get_parent().get_node('../DungeonLayout/DungeonTileMap').get_path(position, target_position)
		#path = get_node('../DungeonLayout/DungeonTileMap').get_path(position, target_position)
		
		if not path or len(path) == 1:
			_change_state(IDLE)
			return
		# The index 0 is the starting cell
		# we don't want the character to move back to it in this example
		target_point_world = path[1]
	_state = new_state

func _process(delta):
	if displayObj !=null:
		displayObj.position = position
	else:
		displayObj = DisplayClass.instance()
		get_node('/root/Game/Scene/Characters').add_child(displayObj)
	
	if not _state == FOLLOW:
		return
	var arrived_to_next_point = move_to(target_point_world)
	if arrived_to_next_point:
		path.remove(0)
		if len(path) == 0:
			_change_state(IDLE)
			return
		target_point_world = path[0]
		#print(target_point_world)


func move_to(world_position):
	var MASS = 10.0
	var ARRIVE_DISTANCE = 10.0

	var desired_velocity = (world_position - position).normalized() * SPEED
	var steering = desired_velocity - velocity
	velocity += steering / MASS
	position += velocity * get_process_delta_time()
	rotation = velocity.angle()
	return position.distance_to(world_position) < ARRIVE_DISTANCE


func _input(event):
	"""
	if event.is_action_pressed('click'):
		if Input.is_key_pressed(KEY_SHIFT):
			global_position = get_global_mouse_position()
		else:
			target_position = get_global_mouse_position()
		_change_state(FOLLOW)
	"""
	if event.is_action_pressed('select'):
		if bHover:
			bSelect = true
			#print("select unit")
			print(dungeonmanagement)
			dungeonmanagement.addUnit(self)
		else:
			if Input.is_key_pressed(KEY_SHIFT):
				dungeonmanagement.addUnit(self)
				pass
			else:
				bSelect = false
			#print("deselect unit")
		pass
	if event.is_action_pressed('move'):
		if bSelect:
			target_position = get_global_mouse_position()
			_change_state(FOLLOW)
			#print("move?")
		pass	
	pass
	
func _on_KB2D_Character_mouse_entered():
	bHover = true
	#print("hover over")
	pass # replace with function body


func _on_KB2D_Character_mouse_exited():
	bHover = false
	pass # replace with function body
