extends Control

var curPos = Vector2()
var builditemobj = null

var objCamera = null

var screen_size
var blocksize = 32
var bfixed = true
var bbuild = false

var LMouseX = 0
var LMouseY = 0

#get_tree().get_root() # Access via scene main loop.
#get_node("/root") # Access via absolute path.

var gcamera2d = null

var dungeonlayout
var CBCollision


var PPHWall01 = preload("res://placeholders/PHWall01.tscn")
var PPHFloor01 = preload("res://placeholders/PHFloor01.tscn")

var placeholders

var PHWall01
var PHFloor01
var BuildMenuPanel

var dropdownmenu
var bOverMenu = false
var bSnap = true

var buildtypes = ["Blocks","Traps","Creatures","Logics","Items"]

var dungeonHUD


func _ready():
	set_process_input(true)
	screen_size = self.get_viewport_rect().size
	objCamera = get_node("/root/Game/Scene/DungonSpiritCamera2D");
	gcamera2d = get_node("/root/Game/Scene/DungonSpiritCamera2D")
	
	dungeonHUD = get_node("/root/Game/HUD/Ctrl_Dungeon_HUD")
	
	dungeonlayout = get_node("/root/Game/Scene/DungeonLayout")
	
	CBCollision = get_node("CBuildMenu/CBCollision")
	CBCollision.connect("toggled",self,"ToggleCollsion")
	
	#pass
	placeholders = get_node("/root/Game/PlaceHolders")
	LMouseX = get_node("LMouseX")
	LMouseY = get_node("LMouseY")
	PHWall01 = PPHWall01.instance()
	PHWall01.hide()
	placeholders.add_child(PHWall01)
	PHFloor01 = PPHFloor01.instance()
	PHFloor01.hide()
	placeholders.add_child(PHFloor01)
	#buildpanel = get_node("Container")
	
	BuildMenuPanel = get_node("CBuildMenu")
	
	dropdownmenu = get_node("CBuildMenu/OBtnSelectBuild")
	dropdownmenu.add_item("Blocks")
	dropdownmenu.add_item("Traps")
	dropdownmenu.add_item("Creatures")
	dropdownmenu.add_item("Logics")
	dropdownmenu.add_item("Items")
	
	dropdownmenu.connect("item_selected",self,"on_item_selectedbuild")
	
	#bSnap
	get_node("CBuildMenu/CBtnSnap").set_pressed(bSnap)
	#print(get_node("CBuildMenu/CBtnSnap").pressed)
	
func ToggleCollsion(value):
	print(value)
	pass
	
func on_item_selectedbuild(id):
	print(str(dropdownmenu.get_item_text(id)))
	
	pass
	
func _input(event):
	#if(event is InputEventKey):
		#if event.pressed and event.scancode == KEY_Z:
			#curPos.x = curPos.x - 1
			#print("Z")
		#if event.pressed and event.scancode == KEY_X:
			#curPos.x = curPos.x + 1
			#print("X")
		#if event.pressed and event.scancode == KEY_A:
			#curPos.y = curPos.y - 1
			#print("A")
		#if event.pressed and event.scancode == KEY_S:
			#curPos.y = curPos.y + 1
			#print("S")
	#if Input.is_action_pressed("Left"):
		#print("test")
		
	#BuildMenuPanel.size
	
	bOverMenu = false
	
	#var checkpoint = BuildMenuPanel.has_point(get_global_mouse_position())
	if BuildMenuPanel.get_rect().has_point(get_global_mouse_position()):
		bOverMenu = true
	if dungeonHUD.get_OverMenu():
		bOverMenu = true
	#print(checkpoint)
		
	if event is InputEventMouseButton:
		if builditemobj != null and bOverMenu == false:
			if event.button_index == 1 and !event.pressed:
				#PHWall0.position = Vector2(gcamera2d.gmousepos.x,gcamera2d.gmousepos.y)
				var tmp_pos = Vector2(gcamera2d.gmousepos.x,gcamera2d.gmousepos.y) #copy position
				if bSnap:#check for object snap
					tmp_pos = Vector2(floor(tmp_pos.x/blocksize) * blocksize,floor(tmp_pos.y/blocksize) * blocksize)
				if checkplaceobject(tmp_pos):#check scene object snap object grid
					print("not placeable")
				else:# place object
					print("placeable!")
					var PHWall0 = PPHWall01.instance()
					get_node("/root/Game/Scene/DungeonLayout").add_child(PHWall0)
					PHWall0.position = tmp_pos
			if event.button_index == 2 and !event.pressed:
				var tmp_pos = Vector2(gcamera2d.gmousepos.x,gcamera2d.gmousepos.y)
				if bSnap:
					tmp_pos = Vector2(floor(tmp_pos.x/blocksize) * blocksize,floor(tmp_pos.y/blocksize) * blocksize)
				if checkdeleteobject(tmp_pos):
					pass
				print("left mouse")
		#print("press")
		pass
	if(event is InputEventMouseMotion):
		#print("move?")
		#print("Mouse Click/Unclick at: ", event.position)
		if objCamera != null:
			var camerapos = objCamera.position # object center pawn
			if builditemobj != null:
				#var gmouse2d = get_global_mouse_position()
				#var mouse2d = get_viewport().get_mouse_position() + get_global_mouse_position()
				#builditemobj.position = (objCamera.position - screen_size / 2.0) + mouse2d
				if gcamera2d != null:
					LMouseX.text = "X:" + str(gcamera2d.gmousepos.x)
					LMouseY.text = "Y:" + str(gcamera2d.gmousepos.y)
					var tmp_pos = gcamera2d.gmousepos
					if bSnap:
						tmp_pos = Vector2(floor(tmp_pos.x/blocksize) * blocksize,floor(tmp_pos.y/blocksize) * blocksize)
					builditemobj.position = tmp_pos
					#print("test??")
				#builditemobj.position = mouse2d
				#pass
			#print(camerapos)
		
		pass
func checkplaceobject(pos):
	var bcheck = false
	#dungeonlayout
	for dunobj in dungeonlayout.get_children():
		#print("checking...",dunobj.name)
		#print(pos)
		#print(dunobj.position)
		if pos.x == dunobj.position.x and pos.y == dunobj.position.y:
			#print("found")
			bcheck = true
			break
		#pass
	return bcheck
	
func checkdeleteobject(pos):
	var bcheck = false
	#dungeonlayout
	for dunobj in dungeonlayout.get_children():
		#print("checking...",dunobj.name)
		#print(pos)
		#print(dunobj.position)
		if pos.x == dunobj.position.x and pos.y == dunobj.position.y:
			#print("found")
			dunobj.queue_free()
			bcheck = true
			break
		#pass
	return bcheck
	
func hidebuildmode():
	if builditemobj != null:
		builditemobj.hide()
		builditemobj = null
	
func hideitem():
	PHWall01.hide()
	PHFloor01.hide()
	pass

func btn_floor_pressed():
	hideitem()
	PHFloor01.show()
	builditemobj = PHFloor01
	print("floor")
	pass 
	
func _on_BtnWall_pressed():
	hideitem()
	PHWall01.show()
	builditemobj = PHWall01
	print("wall")
	pass 
	
func _on_BtnDoor_pressed():
	hideitem()
	print("door")
	pass 

func _on_BtnSpike_pressed():
	hideitem()
	print("spike trap")
	pass 

func _on_BtnArrow_pressed():
	hideitem()
	print("arrow")
	pass 

func _on_BtnSummon_pressed():
	hideitem()
	print("summon")
	pass 

func _on_BtnSlime_pressed():
	hideitem()
	print("slime")
	pass 

#func BtnBack_pressed():
	#hideitem()
	#print("back")
	#pass 

func BtnNone_pressed():
	hideitem()
	pass 

func CBuildMenu_mouse_entered():
	#print("enter")
	pass # replace with function body


func CBuildMenu_mouse_exited():
	#print("exit")
	pass # replace with function body

func CBtnSnap_toggled(button_pressed):
	print(button_pressed)
	bSnap = button_pressed
	pass # replace with function body
