extends Control

var menu_ui
var menuBuild
var menuCreatures
var menuShop
var menuResearch

var panelMenu

var currentmenu
var bOverMenu = false

func _ready():
	menu_ui = get_node("/root/Game/HUD/Ctrl_Menu_UI")
	menuBuild = get_node("/root/Game/HUD/Ctrl_Build_UI")
	menuCreatures = get_node("/root/Game/HUD/Ctrl_Creatures_UI")
	menuShop = get_node("/root/Game/HUD/Ctrl_Shop_UI")
	menuResearch = get_node("/root/Game/HUD/Ctrl_Research_UI")
	panelMenu = get_node("PMenu")
	set_process_input(true)
	pass
	
func _input(event):
	bOverMenu = false
	if panelMenu.get_rect().has_point(get_local_mouse_position()):
		bOverMenu = true
		#print("over")
	#else:
		#print("out")
		
func get_OverMenu():
	return bOverMenu
		
func hidemenus(assinmenu):
		
	if currentmenu != null:
		if currentmenu != assinmenu:
			currentmenu.hide()
			currentmenu = assinmenu
			currentmenu.show()
		else:
			if currentmenu.is_visible():
				currentmenu.hide()
			else:
				currentmenu.show()
		#pass
	else:
		
		currentmenu = assinmenu
		currentmenu.show()
	#pass
func _on_BtnMenu_pressed():
	#print("build")
	hidemenus(menu_ui)
func _on_BtnBuild_pressed():
	#print("build")
	hidemenus(menuBuild)
	if currentmenu.is_visible() == false:
		menuBuild.hidebuildmode()
func _on_BtnShop_pressed():
	#print("Shop")
	hidemenus(menuShop)
	
func _on_BtnCreatures_pressed():
	#print("Creatures")
	hidemenus(menuCreatures)

func _on_BtnResearch_pressed():
	#print("Research")
	hidemenus(menuResearch)
	pass
