tool
extends EditorPlugin

var dock = null
var windowmodes = ["Window","Borderless","Fullscreen"]
var windowmode 
var ScreenResolutions = ["800x600","1280x720"]
var ScreenResolution

var ToggleSubtitles

var Languages = ["English","Japanese"]
var Language

var HSMasterVolume
var SBMasterVolume

func _enter_tree():
	# When this plugin node enters tree, add the custom type

	dock = preload("res://addons/gameconfigs/gameconfigs_dock.scn").instance()

	add_control_to_dock( DOCK_SLOT_LEFT_UL, dock )
	
	dock.get_node("BtnSave").connect("pressed",self, "BtnSave_Pressed")
	dock.get_node("BtnLoad").connect("pressed",self, "BtnLoad_Pressed")
	windowmode = dock.get_node("OBtWindowMode")
	ScreenResolution = dock.get_node("OBtnScreenResolution")
	for wlist in windowmodes:
		#print(wlist)
		windowmode.add_item(wlist)
		pass
		
	for slist in ScreenResolutions:
		#print(slist)
		ScreenResolution.add_item(slist)
		pass
	windowmode.connect("item_selected",self,"OSelectWindow")
	ScreenResolution.connect("item_selected",self,"OSelectScreenResolution")
	
	Language = dock.get_node("OBLanguage")
	for llist in Languages:
		#print(slist)
		Language.add_item(llist)
		pass
	Language.connect("item_selected",self,"OSelectLanguage")
	
	HSMasterVolume = dock.get_node("HSMasterVolume")
	HSMasterVolume.connect("value_changed",self,"HS_MasterVolume")
	SBMasterVolume = dock.get_node("SBMasterVolume")
	SBMasterVolume.connect("value_changed",self,"SB_MasterVolume")
	
	
	ToggleSubtitles = dock.get_node("CBSubtitles")
	#ToggleSubtitles.connect("value_changed",self,"CheckSubtitle")
	ToggleSubtitles.connect("toggled",self,"ToggleCheckSubtitle")
	

func ToggleCheckSubtitle(value):
	print(value)
	pass
	
func HS_MasterVolume(value):
	print(value)
	SBMasterVolume.value = value
	pass
	
func SB_MasterVolume(value):
	HSMasterVolume.value = value
	print(value)
	pass
	
	
func _exit_tree():
	# Remove from docks (must be called so layout is updated and saved)
	remove_control_from_docks(dock)
	# Remove the node
	dock.free()
	
func GSaveData():
	pass
	
func GLoadData():
	pass
	
func BtnSave_Pressed():
	print("save?")
	pass

func BtnLoad_Pressed():
	print("load?")
	#pass
	
func OSelectWindow(id):
	print("OSelectWindow")
	print(id)
	#pass
	
func OSelectScreenResolution(id):
	print("OSelectScreenResolution")
	print(id)
	#pass
	
func OSelectLanguage(id):
	#print("OSelectScreenResolution")
	#print(id)
	print(Languages[id])
	#pass